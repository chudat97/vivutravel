package com.neos.touristbook.view.event;

public interface OnActionCallback {
    void callback(String key, Object data);
}
